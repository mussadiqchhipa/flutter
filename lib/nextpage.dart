import 'package:flutter/material.dart';

class NextPage extends StatelessWidget {
  final String test;

  NextPage({this.test});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(test) ,
        titleSpacing: -10.0,
      ),
      body: Container(
        child: Center(
          child: Text(
            test,
            style: TextStyle(fontSize: 50.0),
          ),
        ),
      ),
    );
  }
}
