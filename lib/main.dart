import 'package:english_words/english_words.dart';
import 'package:flutter/material.dart';
import 'nextpage.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  String title = "list view example";
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: title,
      theme: ThemeData(
        primaryColor: Colors.pink,
        accentColor: Colors.orange,

      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body: RandomWords(title),
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}

class RandomWords extends StatefulWidget {
  final String title;
  RandomWords(this.title);
  @override
  _RandomWordsState createState() => _RandomWordsState();
}

class _RandomWordsState extends State<RandomWords> {
  var _sugesstions = <WordPair>[];

  Widget _buildTile(String word){
    return ListTile(
      leading: CircleAvatar(
        child: Text(word[0]),
      ),
      title: Text(word),
      subtitle: Text(word),
      onTap: (){
        Navigator.push(context, 
        MaterialPageRoute(
          builder: (_) => NextPage(test: word,)
        )
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (context,i) {

        if(i >= _sugesstions.length) {
          print("[loading] => ${i.toString()}");
          _sugesstions.addAll(generateWordPairs().take(10));
        }
        return _buildTile(_sugesstions[i].asUpperCase);
      },
    );
  }
}

